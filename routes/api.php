<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('v1')->group(function () {
	Route::post('login', 'Auth\LoginController@login');

	Route::get('publishers', 'PublisherController@index');
	Route::get('authors', 'AuthorController@index');
	Route::get('authors/{id}/articles', 'AuthorController@articles');
	Route::get('articles', 'ArticleController@index');
	Route::get('articles/latestPublishDate', 'ArticleController@latestPublishDate');
	Route::put('articles/{article}/read', 'ArticleController@read');

	Route::get('users/followings', 'UserAuthorFollowingController@index')->middleware('auth:api');
	Route::post('users/followings', 'UserAuthorFollowingController@store')->middleware('auth:api');
	Route::get('users/followings/all', 'UserAuthorFollowingController@getAllIds')->middleware('auth:api');
	Route::delete('users/followings/{author}', 'UserAuthorFollowingController@destroy')->middleware('auth:api');

	Route::get('users/favorites', 'UserArticleFavoriteController@index')->middleware('auth:api');
	Route::post('users/favorites', 'UserArticleFavoriteController@store')->middleware('auth:api');
	Route::get('users/favorites/all', 'UserArticleFavoriteController@getAllIds')->middleware('auth:api');
	Route::delete('users/favorites/{article}', 'UserArticleFavoriteController@destroy')->middleware('auth:api');

	Route::get('users/lists', 'UserArticleListController@index')->middleware('auth:api');
	Route::post('users/lists', 'UserArticleListController@store')->middleware('auth:api');
	Route::get('users/lists/all', 'UserArticleListController@getAllIds')->middleware('auth:api');
	Route::delete('users/lists/{article}', 'UserArticleListController@destroy')->middleware('auth:api');
});

