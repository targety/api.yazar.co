<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
	public function articles()
	{
		return $this->hasMany('App\Article');
	}

    public function publishers()
    {
    	return $this->belongsToMany('App\Publisher', 'author_archives', 'author_id', 'publisher_id');
    }
}
