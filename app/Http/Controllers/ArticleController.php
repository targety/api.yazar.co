<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\Article as ArticleResource;

class ArticleController extends ApiController
{
    public function index(Request $request)
    {
    	if ($request->has('publish_date')) {
    		$publish_date = $request->query('publish_date');
    	} else {
    		$article = Article::orderByDesc('publish_date')->first('publish_date');
    		$publish_date = $article->publish_date;
    	}

    	return ArticleResource::collection(Article::with(['author', 'publisher'])->where('publish_date', $publish_date)->get());
    }

    public function read(Article $article)
    {
    	$article->increment('total_reading');

    	return response()->json(null, 204);
    }

    public function latestPublishDate()
    {
        $article = Article::latest('publish_date')->first();
        $publish_date = $article->publish_date;

        return response()->json(['publish_date' => $publish_date], 200);
    }
}
