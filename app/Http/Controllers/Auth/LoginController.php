<?php

namespace App\Http\Controllers\Auth;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;

class LoginController extends ApiController
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        $credentials = $request->only('email', 'password');

        if(!Auth::attempt($credentials)) {
            return $this->errorResponse('Unauthorized!', 401);
        }

        $client = DB::table('oauth_clients')->where('password_client', true)->first();

        if (!$client) {
            return $this->errorResponse('Laravel Passport is not setup properly.', 500);
        }

        $data = [
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => request('email'),
            'password' => request('password'),
        ];

        $request = Request::create('/oauth/token', 'POST', $data);

        $response = app()->handle($request);

        if ($response->getStatusCode() != 200) {
            return $this->errorResponse('Wrong email or password', 422);
        }

        $data = json_decode($response->getContent());

        return response()->json([
            'access_token' => $data->access_token
        ]);
    }
}
