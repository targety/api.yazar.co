<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Article as ArticleResource;

class UserArticleFavoriteController extends Controller
{
    public function index()
    {
    	return ArticleResource::collection(Auth::user()->favorites()->with('author.publishers')->orderByDesc('id')->paginate(20));
    }

    public function getAllIds()
    {
        return Auth::user()->favorites()->get()->pluck('id');
    }

    public function store(Request $request)
    {
    	$article = Article::findOrFail($request->input('article_id'));

    	$request->user()->favorites()->syncWithoutDetaching($article);

    	return response()->json(null, 204);
    }

    public function destroy(Article $article)
    {
    	Auth::user()->favorites()->detach($article);

    	return response()->json(null, 204);
    }
}
