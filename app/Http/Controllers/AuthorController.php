<?php

namespace App\Http\Controllers;

use App\Author;
use App\Article;
use App\AuthorArchive;
use Illuminate\Http\Request;
use App\Http\Resources\Author as AuthorResource;
use App\Http\Resources\Article as ArticleResource;

class AuthorController extends Controller
{
    public function index(Request $request)
    {
    	$ids = $request->input('publishers');

    	if($ids) {
    		$ids = explode(',', $ids);

    		return AuthorResource::collection(Author::with('publishers')
    			->whereHas('publishers', function ($query) use ($ids) {
	        	$query->whereIn('publishers.id', $ids);
	    	})->orderByRaw('name COLLATE utf8_turkish_ci asc')->paginate(20)->appends(request()->query()));
    	}

    	return AuthorResource::collection(Author::with('publishers')->orderByRaw('name COLLATE utf8_turkish_ci asc')->paginate(20));    	
    }

    public function articles($id)
    {
    	return ArticleResource::collection(Article::with('publisher')->where('author_id', $id)->orderBy('publish_date', 'desc')->paginate(10));
    }
}
