<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Author as AuthorResource;

class UserAuthorFollowingController extends Controller
{
    public function index()
    {
    	return AuthorResource::collection(Auth::user()->followings()->with('publishers')->orderByRaw('name COLLATE utf8_turkish_ci asc')->paginate(20));
    }

    public function getAllIds()
    {
        return Auth::user()->followings()->get()->pluck('id');
    }

    public function store(Request $request)
    {
    	$author = Author::findOrFail($request->input('author_id'));

    	$request->user()->followings()->syncWithoutDetaching($author);

    	return response()->json(null, 204);
    }

    public function destroy(Author $author)
    {
    	Auth::user()->followings()->detach($author);

    	return response()->json(null, 204);
    }
}
