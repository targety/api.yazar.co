<?php

namespace App\Http\Controllers;

use App\Publisher;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\Publisher as PublisherResource;

class PublisherController extends ApiController
{
    public function index()
    {
        return PublisherResource::collection(Publisher::orderBy('name')->get());
    }

    // public function show(Publisher $publisher)
    // {
    // 	return new PublisherResource($publisher);
    // }

    // public function store(Request $request)
    // {
    // 	$rules = [
    // 		'name' => 'required|unique:publishers',
    // 		'slug' => 'required',
    // 		'all_authors_url' => 'required'
    // 	];

    // 	$this->validate($request, $rules);

    // 	$data = $request->all();

    // 	$publisher = Publisher::create($data);

    // 	return $this->showOne($publisher, 201);
    // }
}
