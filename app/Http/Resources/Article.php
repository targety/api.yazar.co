<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Publisher as PublisherResource;

class Article extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'author_id' => $this->author_id,
            'publisher_id' => $this->publisher_id,
            'title' => $this->title,
            'description' => $this->description,
            'article_url' => $this->article_url,
            'publish_date' => $this->publish_date,
            'total_reading' => $this->total_reading,
            'author' => [
                'id' => $this->author->id,
                'name' => $this->author->name,
                'image' => $this->author->image,
            ],
            'publisher' => new PublisherResource($this->publisher)
        ];
    }
}
