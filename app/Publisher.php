<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    protected $fillable = ['name', 'slug', 'image', 'all_authors_url'];

    protected $casts = [
        'author_page_status' => 'boolean',
        'article_page_status' => 'boolean',
    ];
}
